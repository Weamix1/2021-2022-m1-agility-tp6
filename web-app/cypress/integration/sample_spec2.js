// Test d'exemple par défaut :
describe('The Traffic web site home page, loads HP', () => {
    it('successfully loads', () => {
        cy.visit('/')
        cy.url().should('include', '/')
    })
})

// Test scenario 1
describe('Click on configuration tab, loads configuration page', () => {
    it('successfully loads', () => {
        goOnTab('configuration');
        cy.url().should('include', '/#configuration')
    })
})

// Test scenario 2
describe('On configuration tab, check segments table have 28 segments', () => {
    it('successfully loads', () => {
        goOnTab('configuration');
        cy.get('#segmentsListOnConfigurationPage > tbody > tr').should('have.length', 28)
    })
})

//Test scenario 3
describe('On configuration tab, check the number of vehicles is 0', () => {
    it('successfully loads', () => {
        goOnTab('configuration');
        checkNoVehicleAvailable();
    })
})

//Test scenario 4
describe('On configuration tab, augment speed to 30 for the segment 5', () => {
    it('successfully loads', () => {
        goOnTab('configuration');
        cy.get('#inputSpeedSegment-5').clear().type('30')

        cy.get('#buttonUpdateSegment-5').click()

        cy.request('GET', 'http://127.0.0.1:4567/elements').then((response) => {
            expect(response.body['segments']['4']).to.have.property('speed', 30)
        })

        closeModalDialogAndCheckIsClose()
    })
})
//Test scenario 5 : edit capacity to 4 and duration to 5 for roundabout
describe('On configuration tab, edit capacity to 4 and duration to 15 for roundabout', () => {
    it('successfully loads', () => {
        // update roundabout values
        goOnTab('configuration');
        cy.get('#inputCapacityRoundabout-31').clear().type('15')
        cy.get('#inputDurationRoundabout-31').clear().type('4')
        cy.get('#buttonUpdateRoundabout-31').click()
        closeModalDialogAndCheckIsClose();

        // refresh page and check values (client side)
        cy.reload()
        goOnTab('configuration');
        cy.get('#inputCapacityRoundabout-31').should('have.value', '15')
        cy.get('#inputDurationRoundabout-31').should('have.value', '4')

        // check values on endpoint elements (back side)
        cy.request('GET', 'http://127.0.0.1:4567/elements').then((response) => {
            expect(response.body['crossroads']['2']).to.have.property('capacity', 15)
            expect(response.body['crossroads']['2']).to.have.property('duration', 4)
        })

    })
})

// Test scenario 6 : edit trafficlight id 29 to orange duration to 4, green duration to 40 and next passage duration to 8
describe('On configuration tab, edit trafficlight id 29 to orange duration to 4, green duration to 40 and next passage duration to 8', () => {
    it('successfully loads', () => {
        // update trafficlight values
        goOnTab('configuration');
        cy.get('#inputOrangeDurationTrafficLight-29').clear().type('4')
        cy.get('#inputGreenDurationTrafficLight-29').clear().type('40')
        cy.get('#inputNextPassageDurationTrafficLight-29').clear().type('8')
        cy.get('#buttonUpdateTrafficLight-29').click()
        closeModalDialogAndCheckIsClose()

        // refresh page and check values (client side)
        cy.reload()
        goOnTab('configuration');
        cy.get('#inputOrangeDurationTrafficLight-29').should('have.value', '4')
        cy.get('#inputGreenDurationTrafficLight-29').should('have.value', '40')
        cy.get('#inputNextPassageDurationTrafficLight-29').should('have.value', '8')

        // check values on endpoint elements (back side)
        cy.request('GET', 'http://127.0.0.1:4567/elements').then((response) => {
            expect(response.body['crossroads']['0']).to.have.property('orangeDuration', 4)
            expect(response.body['crossroads']['0']).to.have.property('greenDuration', 40)
            expect(response.body['crossroads']['0']).to.have.property('nextPassageDuration', 8)
        })
    })
})

// Test scenario 7 : Add 3 vehicles with origin S5, destination S26 and start 50
describe('On configuration tab, add 3 vehicles', () => {
    it('successfully loads', () => {

        add3vehiclesOnConfigurationTab(
            'S5', 'S26', '50',
            'S19', 'S8', '200',
            'S27','S2','150');

        // check data on endpoint vehicles (back side)
        cy.request('GET', 'http://127.0.0.1:4567/vehicles').then((response) => {
            expect(response.body['200.0']['0']).to.have.property('position', 0)
            expect(response.body['150.0']['0']).to.have.property('position', 0)
            expect(response.body['50.0']['0']).to.have.property('position', 0)
        })

        // on simulation tab : check there is 3 vehicles
        goOnTab('simulation');
        cy.get('#vehiclesListOnSimulationPage').find('tbody>tr').should('have.length', 3)
    })
})

// Test scenario 8 : Check on simulation tab, that the vehicles are not moving
describe('On simulation tab with a simulation of 120 seconds, check that the vehicles are not moving, except the first one at the end of the simulation', () => {
    it('successfully loads', () => {

        // check that the vehicles are not moving
        checkVehiclesAreNotMoving();
        runSimulationOnSimulationTab(120);
        timeOutAncCheckProgressBarIsFull(15000);

        //check only first vehicle of tables vehicles is moving
        cy.get('#vehiclesListOnSimulationPage').find('tbody > :nth-child(3) > :nth-child(6)').should('have.text','play_circle_filled')
    })
})

// Test scenario 9 :
describe('On simulation tab with a new simulation of 500 seconds, check that the vehicles are not moving at the end of simulation', () => {
    it('successfully loads', () => {
        initSimulationAndRefreshPage();
        add3vehiclesOnConfigurationTab(
            'S5', 'S26', '50',
            'S19', 'S8', '200',
            'S27','S2','150');
        runSimulationOnSimulationTab(500);
        timeOutAncCheckProgressBarIsFull(60000);
        checkVehiclesAreNotMoving();
    })
})

// Test scenario 10 :
describe('On simulation tab with a new simulation of 200 seconds, check position of the vehicles at the end of simulation', () => {
    it('successfully loads', () => {
        initSimulationAndRefreshPage();
        checkNoVehicleAvailable();
        add3vehiclesOnConfigurationTab(
            'S5', 'S26', '50',
            'S5', 'S26', '80',
            'S5','S26','80');
        runSimulationOnSimulationTab(200);
        timeOutAncCheckProgressBarIsFull(25000);

        // check vehicles is on segment 17 & vehicle 2 + 3 are on segment 26
        cy.get('#vehiclesListOnSimulationPage').find('tbody > :nth-child(3) > :nth-child(5)').should('have.text','17')
        cy.get('#vehiclesListOnSimulationPage').find('tbody > :nth-child(2) > :nth-child(5)').should('have.text','29')
        cy.get('#vehiclesListOnSimulationPage').find('tbody > :nth-child(1) > :nth-child(5)').should('have.text','29')


    })
})

function addVehicle(origin, destination,time) {
    cy.get('#inputOriginAddVehicle').clear().type(origin)
    cy.get('#inputDestinationAddVehicle').clear().type(destination)
    cy.get('#inputTimeAddVehicle').clear().type(time)
    cy.get('#buttonAddVehicle').click()
    closeModalDialogAndCheckIsClose()
}

function closeModalDialogAndCheckIsClose() {
    cy.get('#buttonCloseModalDialog').click()
    cy.get('#modalDialog').should('not.be.visible')
}

function add3vehiclesOnConfigurationTab(origin1,destination1, time1,
                                        origin2, destination2, time2,
                                        origin3, destination3, time3) {
    // on configuration tab
    goOnTab('configuration');

    // add vehicle 1 : origin1 -> destination1 with start time time1
    addVehicle(origin1, destination1, time1)

    // add vehicle 2 : origin2 -> destination2 with start time time2
    addVehicle(origin2, destination2, time2)

    // add vehicle 3 : origin3 -> destination3 with start time time3
    addVehicle(origin3, destination3, time3)
}

function checkVehiclesAreNotMoving() {
    cy.get('#vehiclesListOnSimulationPage').find('tbody>tr').each(($el) => {
        cy.wrap($el).find('th>div>span').should('have.text', 'block')
    })
}

function runSimulationOnSimulationTab(seconds) {
    // on simulation tab
    goOnTab('simulation');
    // run simulation of X seconds
    cy.get('#inputTimeRunSimulation').clear().type(seconds)
    cy.get('#buttonRunSimulation').click()
}

function timeOutAncCheckProgressBarIsFull(seconds) {
    //time out X seconds
    cy.wait(seconds)
    //check progress bar is 100%
    cy.get('div[class="progress"]>div[role="progressbar"]').should('have.attr', 'aria-valuenow', '100')
}

function initSimulationAndRefreshPage() {
    //call init endpoint to reset simulation
    cy.request('POST', 'http://127.0.0.1:4567/init')
    //refresh page
    cy.reload()
}

function checkNoVehicleAvailable() {
    cy.get('#noVehicleAvailable').should('have.text', 'No vehicle available')
}

function goOnTab(page) {
    cy.get('a[href ="#'+page+'"]').click()
}