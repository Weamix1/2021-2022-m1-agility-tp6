# Traffic web interface

## Personal notes :

### Part 1 :  sample_spec.js
    I've made some functions, there are all at the end of file (after tests) for better understanding.

![part1-screen.png](part1-screen.png)

### Part 2 : sample_spec.2js
![part2-screen.png](part2-screen.png)

## Description

Web interface of traffic simulator which enables to update network parameters and add vehicles.

This interface is used for passing some Cypress End to End test process.

## How to use it

```bash
git clone https://gitlab.com/jbuisine/2021-2022-m1-agility-tp6.git
```

Run the backend REST API:
```
java -jar traffic.jar
```

Download web app dependencies:
```bash
cd web-app
yarn install
```

Then run the app:
```bash
yarn start
```

Go to [http://127.0.0.1:3000](http://127.0.0.1:3000) to see the app home page.

## Running Cypress

```
cd web-app
./node_modules/.bin/cypress open
```